﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace FaceDetectionAndRecognition
{
    public class RecognitionResult
    {
        public Image<Bgr, Byte> faceImage;
        public Rectangle faceRect;
        public string identifiedName;
        public bool recognised;
    }
}