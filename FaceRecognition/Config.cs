﻿namespace FaceDetectionAndRecognition
{
    public static class Config
    {
        //Папка с зарегестрированными фотографиями
        public static string FacePhotosPath = "Source\\Faces\\";
        //Файл каскада Хаара
        public static string HaarCascadePath = "Resources\\haarcascade_frontalface_default.xml";
        //Время между кадрами видеопотока камеры
        public static int TimerResponseValue = 30;
        //Время между кадрами регистрации
        public static int RegistrationTimerResponseValue = 400;
        //Количество регистраций
        public static int RegistrationSamples = 10;
        public static string ImageFileExtension = ".bmp";
        public static int ActiveCameraIndex = 0;//0: Активная камера
    }
}
