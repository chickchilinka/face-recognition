﻿using System.Drawing;
using System.Windows;

namespace FaceDetectionAndRecognition
{
    public partial class FormAddFace : Window
    {
        public delegate void OnAdd(bool toAdd, string name);

        private OnAdd onAdd;
        public FormAddFace()
        {
            InitializeComponent();
        }
        
        public static void OpenForFace(Bitmap image, OnAdd onAdd)
        {
            var form = new FormAddFace
            {
                onAdd = onAdd,
                DetectedFace =
                {
                    Source = ImageUtils.BitmapToImageSource(image)
                }
            };
            form.Show();
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            Hide();
            onAdd(true, FileName.Text);
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            Hide();
            onAdd(false, FileName.Text);
        }
    }
}