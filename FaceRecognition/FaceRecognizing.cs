﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.IO;
using System.Windows;
using System.Linq;
using Emgu.CV.Face;
using Emgu.CV.Util;

namespace FaceDetectionAndRecognition
{
    public class FaceRecognizing
    {
        private readonly CascadeClassifier _haarCascade;
        private readonly LBPHFaceRecognizer _recognizer;
        private readonly VectorOfMat _imageList = new VectorOfMat();
        private readonly List<string> _nameList = new List<string>();
        private readonly VectorOfInt _labelList = new VectorOfInt();
        private Image<Bgr, Byte> _detectedFace;
        public FaceRecognizing(string haarPath, string photosPath)
        {
            if (!File.Exists(haarPath))
            {
                string text = "Невозможно найти файл каскада Хаара:\n\n";
                text += Config.HaarCascadePath;
                MessageBox.Show(text, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            _haarCascade = new CascadeClassifier(haarPath);
            if (!Directory.Exists(photosPath))
            {
                Directory.CreateDirectory(photosPath);
            }
            _recognizer = new LBPHFaceRecognizer();
            UpdateFacesList(photosPath);
        }
        /// <summary>
        /// Обновление базы лиц
        /// </summary>
        /// <param name="haarPath">Путь на файл каскада Хаара</param>
        /// <param name="photosPath">Путь на базу фото</param>
        public void UpdateFacesList(string photosPath)
        {
            _imageList.Clear();
            _nameList.Clear();
            _labelList.Clear();
            var i = 0;
            foreach (var personDirectory in Directory.GetDirectories(photosPath))
            {
                string person = personDirectory.Split('\\').Last();
                _nameList.Add(person);
                List<int> indecies = new List<int>();
                foreach (var filename in Directory.GetFiles(personDirectory))
                {
                    _imageList.Push(new Image<Gray, byte>(filename));
                    indecies.Add(i);
                }
                i++;
                _labelList.Push(indecies.ToArray());
            }
            if (_imageList.Size > 0)
            {
                _recognizer.Train(_imageList, _labelList);
            }
        }
        /// <summary>
        /// Обнаружение и дальнейшее распознавание лица
        /// </summary>
        /// <param name="capture"></param>
        /// <returns></returns>
        public List<RecognitionResult> TryRecogniseFrom(Image<Bgr, Byte> capture)
        {
            List<RecognitionResult> recognitionResults = new List<RecognitionResult>();
            if (capture != null)
            {
                try
                {
                    Image<Gray, byte> grayframe = capture.Convert<Gray, byte>();
                    Rectangle[] faces = _haarCascade.DetectMultiScale(grayframe, 1.2, 10,
                        new System.Drawing.Size(50, 50), new System.Drawing.Size(300, 300));
                    foreach (var face in faces)
                    {
                        RecognitionResult result = new RecognitionResult();
                        result.faceRect = face;
                        _detectedFace = capture.Copy(face);
                        capture.Draw(face, new Bgr(255, 255, 0), 2);
                        RecognizeFace(result);
                        recognitionResults.Add(result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            return recognitionResults;
        }
        /// <summary>
        /// Идентификация лица
        /// </summary>
        /// <param name="recognitionResult"></param>
        private void RecognizeFace(RecognitionResult recognitionResult)
        {
            recognitionResult.faceImage = _detectedFace.Copy();
            recognitionResult.recognised = false;
            if (_imageList.Size != 0)
            {
                //Eigen Face Algorithm
                FaceRecognizer.PredictionResult result = _recognizer.Predict(recognitionResult.faceImage.Convert<Gray, Byte>().Resize(100, 100, Inter.Cubic));
                if (result.Label >= 0 && result.Label < _nameList.Count && result.Distance <= 90 && result.Distance>30)
                {
                    recognitionResult.recognised = true;
                    int percentage = 0;
                    if (result.Distance < 50)
                    {
                        percentage = 100;
                    }
                    else
                    {
                        percentage = (int)-result.Distance + 150;
                    }
                    percentage = Math.Clamp(percentage, 0, 100);
                    recognitionResult.identifiedName = _nameList[result.Label] + " " + percentage + "%";
                }
            }
        }
    }
}