﻿using System;
using System.Drawing;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Threading;
using System.IO;

namespace FaceDetectionAndRecognition
{
    public partial class WfFaceRecognition
    {
        private readonly FaceRecognizing _recognizing;
        private readonly DispatcherTimer _captureTimer;
        private readonly DispatcherTimer _registrationTimer;
        private string _name;
        private int _registrationCounter = 0;
        private readonly VideoCapture _videoCapture;
        private Image<Bgr, Byte>? _lastDetectedFace;
        private readonly List<Label> _labelPool = new List<Label>();
        public string FaceName
        {
            set { } //value;
        }
        public Bitmap CameraCapture
        {
            set => imgCamera.Source = ImageUtils.BitmapToImageSource(value);
        }
        public WfFaceRecognition()
        {
            InitializeComponent();
            _recognizing = new FaceRecognizing(Config.HaarCascadePath, Config.FacePhotosPath);
            _captureTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromMilliseconds(Config.TimerResponseValue)
            };
            _registrationTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromMilliseconds(Config.RegistrationTimerResponseValue)
            };
            _captureTimer.Tick += CaptureTimer_Elapsed;
            _registrationTimer.Tick += Registration_Elapsed;
            _videoCapture = new VideoCapture(Config.ActiveCameraIndex);
            _videoCapture.Set(CapProp.Fps, 30);
            _videoCapture.Set(CapProp.FrameHeight, 480);
            _videoCapture.Set(CapProp.FrameWidth, Width);
        }

        protected override void OnClosed(EventArgs e)
        {
            _captureTimer.Stop();
            _captureTimer.Tick -= CaptureTimer_Elapsed;
            _videoCapture.Stop();
            _videoCapture.Dispose();
            base.OnClosed(e);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _recognizing.UpdateFacesList(Config.FacePhotosPath);
            _captureTimer.Start();
        }
        private void CaptureTimer_Elapsed(object? sender, EventArgs e)
        {
            ProcessFrame();
        }
        private void Registration_Elapsed(object? sender, EventArgs e)
        {
            ProcessFrame();
            if (_lastDetectedFace != null)
            {
                _registrationCounter++;
                SaveFace(_lastDetectedFace);
                if (_registrationCounter >= Config.RegistrationSamples)
                {
                    _registrationCounter = 0;
                    _registrationTimer.Stop();
                    MessageBox.Show("Лицо зарегистрировано.");
                    _recognizing.UpdateFacesList(Config.FacePhotosPath);
                }
            }
        }
        private void NewFaceButton_Click(object sender, RoutedEventArgs e)
        {
            if (_lastDetectedFace == null)
            {
                MessageBox.Show("No face detected.");
                return;
            }

            if (_lastDetectedFace != null)
            {
                var faceCopy = _lastDetectedFace.Copy();
                FormAddFace.OpenForFace(faceCopy.AsBitmap(), (add, name) =>
                {
                    if (add)
                    {
                        _name = name;
                        _registrationTimer.Start();
                        MessageBox.Show("Регистрирую лицо. Не двигайте его в течение " + (Config.RegistrationSamples * Config.RegistrationTimerResponseValue) / 1000 + "с.");
                    }
                });

            }
            else
            {
                MessageBox.Show("Лицо не обнаружено");
            }
        }
        private void SaveFace(Image<Bgr, Byte> image)
        {
            int probesCount = 0;
            if (Directory.Exists(Config.FacePhotosPath + _name))
            {
                probesCount = Directory.GetFiles(Config.FacePhotosPath + _name).Length;
            }
            else
            {
                Directory.CreateDirectory(Config.FacePhotosPath + _name);
            }
            //Сохраняем обнаруженное лицо
            image = image.Convert<Bgr, Byte>().Resize(100, 100, Inter.Cubic);
            image.Save(Config.FacePhotosPath + _name + '\\' + probesCount +
                              Config.ImageFileExtension);
        }
        private void ProcessFrame()
        {
            var bgrFrame = _videoCapture.QueryFrame().ToImage<Bgr, Byte>();
            if (bgrFrame != null)
            {
                var results = _recognizing.TryRecogniseFrom(bgrFrame);
                CameraCapture = bgrFrame.ToBitmap();
                int labelsCount = _labelPool.Count;
                for (int i = 0; i < results.Count - labelsCount; i++)
                {
                    var label = new Label();
                    CameraViewGrid.Children.Add(label);
                    _labelPool.Add(label);
                }
                for (int i = 0; i < _labelPool.Count; i++)
                {
                    _labelPool[i].Background = null;
                    _labelPool[i].Content = "";
                }
                int d = 0;
                foreach (var result in results)
                {
                    DisplayResult(result, _labelPool[d++]);
                }
            }
        }
        private void DisplayResult(RecognitionResult result, Label label)
        {
            _lastDetectedFace = result.faceImage;

            DrawTextUnderRect(result.identifiedName, result.faceRect, label, result.recognised);
        }
        private void DrawTextUnderRect(string text, Rectangle rect, Label label, bool recognised)
        {
            label.HorizontalAlignment = HorizontalAlignment.Left;
            label.VerticalAlignment = VerticalAlignment.Top;
            label.HorizontalContentAlignment = HorizontalAlignment.Left;
            label.VerticalContentAlignment = VerticalAlignment.Top;
            label.Content = recognised ? text:"Не идентифицировано";
            label.Background = recognised ? System.Windows.Media.Brushes.Green : System.Windows.Media.Brushes.Red;
            label.Margin = new Thickness(rect.X, rect.Y + rect.Height, 0, 0);
            label.FontSize = 24f;
            label.FontFamily = new System.Windows.Media.FontFamily("Arial");
        }
    }
}
